package com.univlille.exempledenavigation;

import static android.view.View.INVISIBLE;
import static androidx.navigation.Navigation.findNavController;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;

public class Fragment1 extends Fragment {

    public Fragment1() {
        super(R.layout.fragment1);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button btnF1_F3 = (Button) view.findViewById(R.id.btnF1_F3);
        Button btnF1_Home = (Button) view.findViewById(R.id.btnF1_Home);

        // navigation vers le fragment 3
        btnF1_F3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavController navController = findNavController(requireActivity(), R.id.nav_host_fragment_content_main);
                navController.navigate(R.id.action_fragment1_to_fragment3);
            }
        });

        // retour à l'accueil (on supprime toute la navigation intermédiaire éventuelle)
        btnF1_Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavController navController = findNavController(requireActivity(), R.id.nav_host_fragment_content_main);
                navController.navigate(R.id.accueil);
            }
        });

        int compteur = Fragment1Args.fromBundle(getArguments()).getCompteur();
        if (compteur < 2) {
            Toast.makeText(this.getContext(), "Vous m'avez appelé, me voilà !", Toast.LENGTH_SHORT).show();
        } else if (compteur == 2) {
            Toast.makeText(this.getContext(), "ça fait " + compteur + " fois que vous m'appelez ! A la prochaine, je fais disparaître le bouton de retour !", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this.getContext(), "je vous avais prévenu !!!!", Toast.LENGTH_SHORT).show();
            btnF1_Home.setVisibility(INVISIBLE);
        }
    }
}

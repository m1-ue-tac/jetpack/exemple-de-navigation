package com.univlille.exempledenavigation;

import static androidx.navigation.Navigation.findNavController;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;

public class Fragment2 extends Fragment {

    public Fragment2() {
        super(R.layout.fragment2);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button btnF2_F3 = (Button) view.findViewById(R.id.btnF2_F3);
        Button btnF2_Home = (Button) view.findViewById(R.id.btnF2_Home);

        // navigation vers le fragment 3
        btnF2_F3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavController navController = findNavController(requireActivity(), R.id.nav_host_fragment_content_main);
                navController.navigate(R.id.action_fragment2_to_fragment3);
            }
        });

        // retour à l'accueil (on supprime toute la navigation intermédiaire éventuelle)
        btnF2_Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavController navController = findNavController(requireActivity(), R.id.nav_host_fragment_content_main);
                navController.navigate(R.id.accueil);
            }
        });
    }
}

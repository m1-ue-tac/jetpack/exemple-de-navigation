package com.univlille.exempledenavigation;

import static androidx.navigation.Navigation.findNavController;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

public class FragmentHome extends Fragment {

    /**
     * Information complémentaire : popUpToSaveState and restoreSaveState
     * When you use app:popUpTo to navigate to a destination, Navigation allow you to optionally
     * save the states of all destinations popped off of the back stack. To enable this option,
     * include app:popUpToSaveState="true" in the associated <action> element:
     *
     * <action
     *   android:id="@+id/action_c_to_a"
     *   app:destination="@id/a"
     *   app:popUpTo="@+id/a"
     *   app:popUpToInclusive="true"
     *   app:popUpToSaveState="true"/>
     *
     * When you navigate to a destination, you can also include app:restoreSaveState="true" to
     * automatically restore the state associated with the destination in app:destination.
     */

    private int compteur = 0;

    public FragmentHome() {
        super(R.layout.fragment_home);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button btnHome_F1 = (Button) view.findViewById(R.id.btnHome_F1);
        Button btnHome_F2 = (Button) view.findViewById(R.id.btnHome_F2);
        Button btnHome_F3 = (Button) view.findViewById(R.id.btnHome_F3);
        Button btnHome_F4 = (Button) view.findViewById(R.id.btnHome_F4);

        // navigation vers le fragment 1 (avec passage de paramètre, ici un compteur (int))
        btnHome_F1.setOnClickListener(new View.OnClickListener()
         // solution 1
        {
            @Override
            public void onClick(View view) {
                compteur++;
                FragmentHomeDirections.ActionFragmentHomeToFragment1 action = FragmentHomeDirections.actionFragmentHomeToFragment1(compteur);
                NavController navController = findNavController(requireActivity(), R.id.nav_host_fragment_content_main);
                navController.navigate(action);
            }
        });

        // solution 2 (avec passage de paramètre, ici un compteur (int))
        //{
        //    @Override
        //    public void onClick(View view) {
        //        compteur++;
        //        findNavController(view).navigate(FragmentHomeDirections.actionFragmentHomeToFragment1(compteur));
        //    }
        //});


        // navigation vers le fragment 2
        btnHome_F2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavController navController = findNavController(requireActivity(), R.id.nav_host_fragment_content_main);
                navController.navigate(R.id.action_fragmentHome_to_fragment2);
            }
        });

        // navigation vers le fragment 3 (pas prévu dans le graphe de navigation, donc code et boutons inutiles !)
        //btnHome_F3.setOnClickListener(new View.OnClickListener()
        //{
        //    @Override
        //    public void onClick(View view) {
        //        NavController navController = findNavController(requireActivity(), R.id.nav_host_fragment_content_main);
        //        navController.navigate(R.id.action_fragmentHome_to_fragment3);
        //    }
        //});

        // navigation vers le fragment 4 (à faire si besoin)
    }
}

package com.univlille.exempledenavigation;

import static androidx.navigation.Navigation.findNavController;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;

public class Fragment3 extends Fragment {

    public Fragment3() {
        super(R.layout.fragment3);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // retour à l'accueil (on supprime toute la navigation intermédiaire éventuelle)
        Button btnF3_Home = (Button) view.findViewById(R.id.btnF3_Home);
        btnF3_Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavController navController = findNavController(requireActivity(), R.id.nav_host_fragment_content_main);
                navController.navigate(R.id.accueil);
            }
        });
    }
}
